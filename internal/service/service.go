package service

import "gitlab.com/fajarcandraaa/short_url/internal/repositories"

type Service struct {
	ShortenedUrlService ShortenedUrlContract
}

func ServiceShortenedUrl(repo *repositories.Repository) ShortenedUrlContract {
	return NewUrlShortenedUrlService(repo)
}

func NewService(repo *repositories.Repository) *Service {
	return &Service{
		ShortenedUrlService: ServiceShortenedUrl(repo),
	}
}
