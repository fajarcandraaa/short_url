package main

import (
	"gitlab.com/fajarcandraaa/short_url/config"
)

func main() {
	config.Run()
}
