package routers

import (
	"gitlab.com/fajarcandraaa/short_url/internal/repositories"
	"gitlab.com/fajarcandraaa/short_url/internal/service"
)

func (se *Serve) initializeRoutes() {
	r := repositories.NewRepository(se.DB) //initiate repository
	s := service.NewService(r)             //initiate service

	//initiate endpoint
	shortUrlRoute(se, s)
}
