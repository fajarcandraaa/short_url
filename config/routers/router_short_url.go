package routers

import (
	"gitlab.com/fajarcandraaa/short_url/internal/service"
	"gitlab.com/fajarcandraaa/short_url/usecase"
)

func shortUrlRoute(se *Serve, s *service.Service) {
	var (
		shortUrlUseCase = usecase.NewShortenedUrlUseCase(s)
	)

	se.Router.HandleFunc("/urlshort", shortUrlUseCase.GenerateShortUrl).Methods("POST")
	se.Router.HandleFunc("/{shorturl}", shortUrlUseCase.RedirectUrl).Methods("GET")
	se.Router.HandleFunc("/urlshort/list", shortUrlUseCase.UrlShortList).Methods("GET")

}
