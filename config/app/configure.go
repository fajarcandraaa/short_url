package app

import "gitlab.com/fajarcandraaa/short_url/internal/entity"

// SetMigrationTable is used to register entity model which want to be migrate
func SetMigrationTable() []interface{} {
	var migrationData = []interface{}{
		&entity.Url{},
	}

	return migrationData
}
